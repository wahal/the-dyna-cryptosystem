#!/usr/bin/env python

#----------------------------------------------------------------------------------#
# Project : The Dyna Cryptosystem                                                  #
# Script  : The ADFGX Cipher                                                       #
# Author  : Mrinal Wahal                                                           #
# Website : http://www.dynacrux.de.vu/                                             #
#----------------------------------------------------------------------------------#

import sys, random, os

print "-"*60
print "The ADFGX Cipher"
print "-"*60

alphas = [letter for letter in "abcdefghjklmnopqrstuvwxyz"]
polybius = {"A":0,"D":1,"F":2,"G":3,"X":4}
sort_poly = [x for x in polybius.iteritems()]
sort_poly.sort(key=lambda x: x[1])

print "\nThe Polybius Being Used:\n"
print " ",
for head in sort_poly: print head[0],

letters = []
for x in range (len(alphas)/5):
    another_list = []
    for y in range (5):
        element = random.choice(alphas)
        if element in letters: pass
        else: another_list.append(element), alphas.remove(element)
    letters.append(another_list)

zero = 0
for key in sort_poly:
    print
    print key[0],
    for letter in letters[zero]: print letter,
    zero += 1

string_input = raw_input("\n\nEnter The Message: ")
string = string_input.lower()

trans_word_input = raw_input("Enter The Transposition Word: ")
trans_word = trans_word_input.lower()

coordinates = []
for let in string:
    if let == "i":
        let = "j"
    for rownum, row in enumerate(letters):
        for colnum, value in enumerate(row):
           if value == let:
               coordinates.append(list((rownum, colnum)))

print "\nPartial Cipher is As Followed: "

storage = []
print
for x in range(len(coordinates)):
    for y in coordinates[x]:
        for key in polybius.iterkeys():
            if polybius[key] == y:
                storage.append(key)
                print key,

len_poly = [ord(x) for x in trans_word]
transposition = [i[0] for i in sorted(enumerate(len_poly), key=lambda x:x[1])]
    
print "\nTransposition Being Used Is As Follwed: \n"
for ext in trans_word: print ext,
print
for elf2 in transposition: print elf2,
print "\n\nSo, You Polybius Is As Followed:"

nonempty = []
len_row = len(trans_word)

for t in range(len(storage)/len_row):
    nonempty.append(list(storage[:len_row]))
    del(storage[:len_row])
if len(storage) != 0: nonempty.append(list(storage[:]))

print
for ext in trans_word: print ext,
print
for elf2 in transposition: print elf2,
print

just_another_list = []
for k in range(len(nonempty)):
    khali = []
    for d in nonempty[k]:
        khali.append(d)
        print d,
    print
    just_another_list.append(khali)

print "\nYour Ciphered Test Is As Followed: "
print

again = [i[0] for i in sorted(enumerate(transposition), key=lambda x:x[1])]

for watson in again:
    for sorry in range(len(just_another_list)):
        try: print just_another_list[sorry][watson],
        except IndexError: print "N",

print "\n"
os.system("pause")